Router.route('/', function(){
	this.render('landingPage');
})

Router.route('/depot', function(){
	this.layout('depotLayout')
	this.render('dashboard');
})

Router.route('/tracking', function(){
	this.layout('depotLayout')
	this.render('tracking')
})

Router.route('/messaging', function(){
	this.layout('depotLayout')
	this.render('messaging')
})

Router.route('/org', function(){
	this.layout('depotLayout')
	this.render('organizations')
})

Router.route('/user_admin', function(){
	this.layout('depotLayout')
	this.render('userAdmin')
})
