


Template.email.helpers({
	isDone: function(){
		return Session.get('done')
	}
})

Template.email.events({
	"submit #signup": function(event){
		var email_address = event.target.lead_email.value
		Meteor.call("sendEmail", email_address)
		Session.set('done', true);
		return false;
	}
})