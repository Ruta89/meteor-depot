if (Meteor.isClient){
	console.log("subscribing to orgs")
	Meteor.subscribe("orgs")
	Meteor.subscribe("users")
}

Template.organizations.events({
	'submit #newOrg':function(event){
		orgName = event.target.inputOrgName.value
		orgAddress = event.target.inputAddress.value
		orgSecAddress = event.target.inputSAddress.value
		orgPOCemail = event.target.SecEmail.value

		Meteor.call("addOrganization", orgName,orgAddress,orgSecAddress,orgPOCemail)
	}
})

Template.userAdmin.helpers({
	orgs: function(){
		return Orgs.find();
	},

	users:function(){
		return Meteor.users.find();
	},

	isDone:function(){
		return Session.get('done')
	}
})

Template.userAdmin.events({
	'submit #newUser':function (event) {
		event.preventDefault();
		userName = event.target.inputUserName.value
		organization = event.target.organizations.value
		userRole = event.target.role.value
		userEmail = event.target.inputUserEmail.value
		userPhone = event.target.inputUserPhone.value
		console.log('this code is reached')
		Meteor.call("addUser", userName,organization,userEmail,userPhone, function(error,result){
			if(error){
				console.log("error",error);
				Session.set('error', true)
			}
			if(result){
				console.log("result",result)
				Session.set('error', false)
				Session.set('done', true)
				Meteor.call("addRole",result,userRole)
			}
		})//end call
	},//end event
	'click .btn-yes':function(){
		Session.set('done', false)
	},
	'click .btn-no':function(){
		Router.go('/depot')
	},
	'click #deleteUser':function(){
		if(window.confirm("Are you sure you want to delete this user from the system")){
			Meteor.call('deleteUser',this._id)
		}
	},


})
