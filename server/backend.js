//user auth https://github.com/alanning/meteor-roles/



if (Meteor.isServer){
	Meteor.startup(function(){
		console.log("server is listening")
	});



	//Meteor.publish some stuff here
	Meteor.methods({
		sendEmail: function(email){

			EmailAddress.insert({
				emailaddress : email,
			})

			Email.send({
				to:email,
				from:'no-reply@depotpls.com',
				subject: 'Thank you for following our project',
				text:"I won't spam you with newsletters or sell your email address. The next email you get from us will be to notify you of the site progress",
			});
		},

		isUserAdmin:function(){
			var loggedInUser = Meteor.user()

			if (loggedInUser || Roles.userIsInRole(loggedInUser,['Admin'])){
				return true;
			}
			else{
				return false;
			}


		}

	});
}
