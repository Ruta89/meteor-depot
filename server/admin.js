

Meteor.methods({

	addOrganization: function(orgName,orgAddress,orgSecAddress,orgPOCemail){

		boolIsAdmin = Meteor.call(isUserAdmin)
		console.log(boolIsAdmin)
		if (boolIsAdmin){
			Orgs.insert({
				orgName:orgName,
				orgAddress:orgAddress,
				orgSecAddress:orgSecAddress,
				orgPOCemail:orgPOCemail
			})
		}
		else{
			throw new Meteor.Error(403,"access denied")
		}

	},

	debug:function(){
		console.log("debug from the server")
	},

	deleteUser:function(userID){
		var loggedInUser = Meteor.user()

		if (loggedInUser || Roles.userIsInRole(loggedInUser,['Admin'])){
			Meteor.users.remove(userID)
		}
		else {
			throw new Meteor.Error(403,"access denied")
		}


	},

	addUser:function(userName,organization,userEmail,userPhone){
		//TO DO: authorization and authentication check

		userOptions = {
				email:userEmail,
				profile: {
					userName:userName,
					organization:organization,
					userPhone:userPhone,
				},
		}
		var userId= Accounts.createUser(userOptions);
		Accounts.sendEnrollmentEmail(userId);
		Meteor.users.insert(userId)
		return userId;



	},

	addRole:function(id,userRole){
		//if user is not admin throw error not authoried
		Roles.addUsersToRoles(id, [userRole]);
	},


});

Meteor.publish("orgs", function(){
	//if user is not admin throw error not authoried

	return Orgs.find()
})

Meteor.publish("users", function(){
	//if user is not admin throw error not authoried
	return Meteor.users.find()
})
