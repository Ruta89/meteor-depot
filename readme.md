## Synopsis

This is a Meteor application that will provide inventory tracking and depot (RMA) management


## Motivation

I had to use a really bad ASP.net classic application for the longest time. Wanted to build a better tool for myself and others

## Installation

Go get Meteor and clone this project into the directory

## API Reference

TBD

## Tests

I know I should write these. I feel really bad about not doing it. Honest.

## Contributors

If you want to get in on this let me know my username is also my twitter handle

## License

Until I figure out what this is:
Copyright (c) 2015 Dylan Zitzmann
[![License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)
